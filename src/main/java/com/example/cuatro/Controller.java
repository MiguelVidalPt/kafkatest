package com.example.cuatro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Autowired
    private KafkaTemplate kafkaTemplate;


    @Autowired
    private KafkaTemplate <String,User> kafkaTemplate2;

    private static final String TOPIC = "Kafka_Example";

    @GetMapping("/jhin")
    String mymessage() {

        kafkaTemplate.send(TOPIC, "hola");
        return "Cuatro";
    }

    @GetMapping("/lux")
    String mymessage2() {
     User x = new User("miguel","nose", (long) 1000);
        kafkaTemplate2.send("Kafka_Example2", x);
        return "Cuatro";
    }

    @KafkaListener(id = "test", topics = "Kafka_Example")
    public void listen(String in) {
        System.out.println(in);
    }

    @KafkaListener(id = "test2", topics = "Kafka_Example2")
    public void listen2(String in) {
        System.out.println(in);
    }
}
